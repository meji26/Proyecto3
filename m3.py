from tkinter import* 
from threading import Thread
import threading
import winsound
import os
import sys
import time
from tkinter import messagebox
import random
global nave
import pygame
from pygame.locals import *
pygame.init()
global x
global y
x=200
y=430
global Gane
Gane=False



root=Tk()
root.title("Star Wars Arcade")
root.minsize(1400,750)
root.resizable(width=NO,height=NO)
C_root=Canvas(root, width=1400, height=750, bg="black")
C_root.place(x=0,y=0)

def cargarImagen(nombre):
    """
    Instituto Tecnologico de Costa Rica
    Ingieneria en Computadores
    Programa:cargarImagen
    Lenguaje Python 3.6.4
    Autor:Daniel Núñez Murillo
    Version 1.0
    Fecha de Ultima Modificacion:Mayo 2/2018
    Entradas:nombre de la imagen
    Salidas:la imagen en el canvas
    Restricciones: la imagen debe encontrarse en la carpeta img
    """
    ruta = os.path.join('img',nombre)
    imagen = PhotoImage(file=ruta)
    return imagen
def song():
    winsound.PlaySound(None,winsound.SND_ASYNC)
p=threading.Thread(target=song,args=())
p.start()
imagenFondo = cargarImagen("gamefondo.gif")
C_root.create_image(0,0, image = imagenFondo, anchor = NW)



def seleccion():
    root.withdraw()
    sele=Toplevel()
    sele.minsize(1000,800)
    sele.resizable(width=NO, height= NO)
    
    C_sele=Canvas(sele, width=1000,height=800, bg='black')
    C_sele.place(x=0,y=0)
    
    
    g1= cargarImagen("i1.gif")
    g2= cargarImagen("i2.gif")
    g3= cargarImagen("i3.gif")
    g4= cargarImagen("4.gif")
    g5= cargarImagen("5.gif")
    g6= cargarImagen("6.gif")
    g7= cargarImagen("7.gif")
    g8= cargarImagen("8.gif")
    g9= cargarImagen("9.gif")
    g10= cargarImagen("10.gif")
    g11= cargarImagen("11.gif")
    g12= cargarImagen("12.gif")
    g13= cargarImagen("13.gif")
    g14= cargarImagen("14.gif")
    g15= cargarImagen("15.gif")
    g16= cargarImagen("16.gif")
    g17= cargarImagen("17.gif")
    g18= cargarImagen("18.gif")
    g19= cargarImagen("19.gif")
    g20= cargarImagen("20.gif")
    def back1():
        
        sele.destroy()
        root.deiconify()

   
    
   
    Btn_back = Button(sele,text='Atras',command=back1,bg='Black',fg='yellow')
    Btn_back.place(x=40,y=750)
    
        
    def disparo():
        
              
        """
    Instituto Tecnologico de Costa Rica
    Ingieneria en Computadores
    Programa:disparo
    Lenguaje Python 3.6.4
    Autor:Daniel Núñez Murillo
    Version 1.0
    Fecha de Ultima Modificacion:Mayo 5/2018
    Entradas:No hay
    Salidas:Se encarga de la creacion de la ventana principal del juego
    Restricciones:No hay
    """
        sele.withdraw()
        #Pantalla secundari
        info=Toplevel()
        info.title('SELECCION DE NIVEL')
        info.minsize(1400,750)
        info.resizable(width=NO, height=NO)
       

        C_info=Canvas(info, width=1400,height=750, bg='black')
        C_info.place(x=0,y=0)
       

        
        def back():
            
            info.destroy()
            sele.deiconify()
        

                   
            

        
        
       
       
        
        
            
       
#_____________________________________________________________________________________
#Variables que permiten que sea más eficiente el control de algunas opciones para el juego
        ANCHO_PANTALLA= 800
        ALTO_PANTALLA= 600
        #_____________________________________________________________________________________
        #___________________________________
        #Se definen los códigos RGB de algunos colores
        white=(255,255,255)
        black=(0,0,0)
            #_____________________________________________________________________________________
            #Se define una clase para crear y manejar el movimiento de la nave en la pantalla


        

        fondo=pygame.image.load('fondo.gif')
        naveImg = pygame.image.load('Nave.gif')
        asteroide = pygame.image.load('Asteroide.png')
        aro = pygame.image.load('aro.png')
        
        
        def pygame1():
            
            def iniciar():
                info.withdraw()
                
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                
                clock= pygame.time.Clock()
                clock.tick(60)#Estos son los fps para que se actualice la pantalla
                pygame.display.update()
           
            
            def nave(x,y):
                info.withdraw()
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                
                clock= pygame.time.Clock()
                clock.tick(200)#Estos son los fps para que se actualice la pantalla
                
                Pantalla.blit(naveImg,(x,y))
                pygame.display.update()
            
                
                
            
            def mov_nave(event):
                global x,y
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.fill(black)
                clock= pygame.time.Clock()
                clock.tick(200)#Estos son los fps para que se actualice la pantalla
                if x+10<450 and event  == "RIGHT":
                    x = x + 10
                    Pantalla.fill(black)
                    pygame.display.update(nave(x,y))
                elif x-10>0 and event == "LEFT":
                    x = x-10
                    Pantalla.fill(black)
                    pygame.display.update(nave(x,y))
                elif y-10>0 and event == "UP":
                    y = y - 10
                    Pantalla.fill(black)
                    pygame.display.update(nave(x,y))
                elif y+10<450 and event == "DOWN":
                    y = y + 10
                    Pantalla.fill(black)
                    pygame.display.update(nave(x,y))  
            def main(Modo):
                iniciar()        
                nave(200,430)
                
                if Modo == "Aros":
                    while nave:
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                pygame.quit()
                                quit()
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_RIGHT:
                                    mov_nave("RIGHT")
                                if event.key == pygame.K_LEFT:
                                    mov_nave("LEFT")
                                if event.key == pygame.K_UP:
                                    mov_nave("UP")
                                if event.key == pygame.K_DOWN:
                                    mov_nave("DOWN")
                                        
                if Modo == "Asteroides":
                    while nave:
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                pygame.quit()
                                quit()
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_RIGHT:
                                    mov_nave("RIGHT")
                                if event.key == pygame.K_LEFT:
                                    mov_nave("LEFT")
                                if event.key == pygame.K_UP:
                                    mov_nave("UP")
                                if event.key == pygame.K_DOWN:
                                    mov_nave("DOWN")
            
                
           
            main("Asteroides")

        def pygame2():
            
            def iniciar():
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                
                clock= pygame.time.Clock()
                clock.tick(60)#Estos son los fps para que se actualice la pantalla
                pygame.display.update()
           
            
            def nave(x,y):
                info.withdraw()
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                
                clock= pygame.time.Clock()
                clock.tick(2000000)#Estos son los fps para que se actualice la pantalla
                Pantalla.blit(naveImg,(x,y))
                
                
                       
            def mov_nave(event):
                    global x,y
                    Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                    pygame.display.set_caption("Pantalla de Juego")
                    Pantalla.blit(naveImg,(x,y))
                    pygame.display.update()
                    

                    
                    if x+10<450 and event  == "RIGHT":
                        x = x + 10
                        
                        
                        pygame.display.update()
                        
                    elif x-10>0 and event == "LEFT":
                        x = x-10
                        
                        
                        pygame.display.update()
                    elif y-10>0 and event == "UP":
                        y = y - 10
                        pygame.display.update()
                    elif y+10<450 and event == "DOWN":
                        y = y + 10
                        
                        pygame.display.update()
                        
            
            def mov_aros(C,T):
                x1=random.randint(1,300)
                y1=random.randint(1,300)
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                pygame.display.update(nave(x,y))
                if T<=8:
                  
                    Pantalla.blit(aro,(x1,y1))
                    pygame.display.update()
                    return mov_aros_aux(C+1,T+1,x1,y1,aro)
                else:
                    
                    
                    pygame.quit()
                    puntajes()
                    
                    
                    
                    
                    

            
            def mov_aros_aux(C,T,x1,y1,im):
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                pygame.display.update(nave(x,y))
              

                if C==5:
                    mov_aros(0,T)
                    print(T)
                    
                else:
                    time.sleep(0.3)
                    aros= pygame.transform.scale2x(im)
                    Pantalla.blit(aros,(x1-50,y1-50))
                    
                    pygame.display.update()
                    #pygame.display.update(nave(x,y))
                    
                    return mov_aros_aux(C+1,T,x1,y1,aros)
                

                
                
                
           
              
                
            def main():
                T=0
                
                Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
                pygame.display.set_caption("Pantalla de Juego")
                Pantalla.blit(fondo,(0,0))
                clock= pygame.time.Clock()
                clock.tick(60)#Estos son los fps para que se actualice la pantalla
                
                
                
                    
                while T<=9:                  
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_RIGHT:
                                mov_nave("RIGHT")
                            if event.key == pygame.K_LEFT:
                                mov_nave("LEFT")
                            if event.key == pygame.K_UP:
                                mov_nave("UP")
                            if event.key == pygame.K_DOWN:
                                mov_nave("DOWN")

                    mov_aros(0,T)
                    pygame.display.update()
                    nave(x,y)

                        

                    
                else:
                    pygame.quit()
                    
                    puntajes()
            
            
                
            main()
            





        def puntajes():
            pun=Toplevel()
            pun.title('CREDITOS')
            pun.minsize(700,800)
            pun.resizable(width=NO, height=NO)
            def bubble(lista):
    
                return bubble_aux(lista,0,0,len(lista),False)
            def bubble_aux(lista,i,j,n,Swap):
                if i==n:
                    return lista
                elif j==n-i-1:
                    if Swap:
                        return bubble_aux(lista,i+1,0,n,False)
                    else:
                        return lista
                elif lista[j]>lista[j+1]:
                    tmp=lista[j]
                    lista[j]=lista[j+1]
                    lista[j+1]=tmp
                    
                    return bubble_aux(lista,i,j+1,n,True)
                else:
                    return bubble_aux(lista,i,j+1,n,Swap)
            C_pun=Canvas(pun, width=710,height=800, bg='black')
            C_pun.place(x=0,y=0)
            lista=[]
           
            def lis(lista):
                for x in range (0, 20):
                    lista.append(random.randint(0, 9))
                else:
                    return lista
            lis(lista)    
            listan=bubble(lista)    
            listap=[E_nombre.get(),E_nombre2.get(),E_nombre3.get(),E_nombre4.get(),E_nombre5.get(),E_nombre6.get(),E_nombre7.get(),E_nombre8.get(),E_nombre9.get(),E_nombre10.get(),E_nombre11.get(),E_nombre12.get(),E_nombre13.get(),E_nombre14.get(),E_nombre15.get(),E_nombre16.get(),E_nombre17.get(),E_nombre18.get(),E_nombre19.get(),E_nombre20.get()]
            listap=random.sample(listap,20)
            L_pun = Label(pun,text=listap[0], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=50)
            L_pun = Label(pun,text=listap[1], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=100)
            L_pun = Label(pun,text=listap[2], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=150)
            L_pun = Label(pun,text=listap[3], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=200)
            L_pun = Label(pun,text=listap[4], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=250)
            L_pun = Label(pun,text=listap[5], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=300)
            L_pun = Label(pun,text=listap[6], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=350)
            L_pun = Label(pun,text=listap[7], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=400)
            L_pun = Label(pun,text=listap[8], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=450)
            L_pun = Label(pun,text=listap[9], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=500)
            L_pun = Label(pun,text=listap[10], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=550)
            L_pun = Label(pun,text=listap[11], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=600)
            L_pun = Label(pun,text=listap[12], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=650)
            L_pun = Label(pun,text=listap[13], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=700)
            L_pun = Label(pun,text=listap[14], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=10,y=750)
            L_pun = Label(pun,text=listap[15], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=300,y=50)
            L_pun = Label(pun,text=listap[16], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=300,y=100)
            L_pun = Label(pun,text=listap[17], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=300,y=150)
            L_pun = Label(pun,text=listap[18], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=300,y=200)
            L_pun = Label(pun,text=listap[19], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=13,height=1)
            L_pun.place(x=300,y=250)
            
            
            L_pun = Label(pun,text=listan[19], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=50)
            L_pun = Label(pun,text=listan[18], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=100)
            L_pun = Label(pun,text=listan[17], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=150)
            L_pun = Label(pun,text=listan[16], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=200)
            L_pun = Label(pun,text=listan[15], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=250)
            L_pun = Label(pun,text=listan[14], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=300)
            L_pun = Label(pun,text=listan[13], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=350)
            L_pun = Label(pun,text=listan[12], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=400)
            L_pun = Label(pun,text=listan[11], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=450)
            L_pun = Label(pun,text=listan[10], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=500)
            L_pun = Label(pun,text=listan[9], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=550)
            L_pun = Label(pun,text=listan[8], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=600)
            L_pun = Label(pun,text=listan[7], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=650)
            L_pun = Label(pun,text=listan[6], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=700)
            L_pun = Label(pun,text=listan[5], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=200,y=750)
            L_pun = Label(pun,text=listan[4], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=500,y=50)
            L_pun = Label(pun,text=listan[3], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=500,y=100)
            L_pun = Label(pun,text=listan[2], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=500,y=150)
            L_pun = Label(pun,text=listan[1], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=500,y=200)
            L_pun = Label(pun,text=listan[0], font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=10,height=1)
            L_pun.place(x=500,y=250)
            L_pun = Label(pun,text="Puntajes", font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=12,height=1)
            L_pun.place(x=200,y=10)
            L_pun = Label(pun,text='Piloto', font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=12,height=1)
            L_pun.place(x=10,y=10)
            L_pun = Label(pun,text="Puntajes", font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=12,height=1)
            L_pun.place(x=500,y=10)
            L_pun = Label(pun,text='Piloto', font=('Agency FB',20) ,bg='black', fg='yellow')
            L_pun.config(width=12,height=1)
            L_pun.place(x=300,y=10)
            def menu():
                pun.destroy()
                root.deiconify()
            Btn_back = Button(pun,text='MENU',bg='Black',command=menu,fg='yellow')
            Btn_back.place(x=505,y=700)    
            Btn_back.config( height = 5, width =20)
            
        imagenGri = cargarImagen("fondo.gif")
        C_info.create_image(0,0, image = imagenGri, anchor = NW)
        Btn_back = Button(info,text='ASTEROIDES',bg='Black',command=pygame1,fg='yellow')
        Btn_back.place(x=595,y=360)    
        Btn_back.config( height = 7, width =70)
        Btn_back = Button(info,text='AROS',bg='Black',command=pygame2,fg='yellow')
        Btn_back.place(x=595,y=160)    
        Btn_back.config( height = 7, width =70)
        Btn_back = Button(info,text='Atras',command=back,bg='Black',fg='yellow')
        Btn_back.place(x=416,y=532)    
        Btn_back.config( height = 1, width =13)
        info.mainloop()
    
    initialtext= StringVar()
    initialtext.set('Darth Vader')
    E_nombre = Entry(sele,textvariable=initialtext,width=9,font=('Agency FB',14))
    E_nombre.place(x=10,y=150)
    initialtext2= StringVar()
    initialtext2.set('Garindan')
    E_nombre2 = Entry(sele,textvariable=initialtext2,width=9,font=('Agency FB',14))
    E_nombre2.place(x=150,y=150)
    initialtext3= StringVar()
    initialtext3.set('Sandtrooper')
    E_nombre3 = Entry(sele,textvariable=initialtext3,width=9,font=('Agency FB',14))
    E_nombre3.place(x=290,y=150)
    initialtext4= StringVar()
    initialtext4.set('Dengar')
    E_nombre4 = Entry(sele,textvariable=initialtext4,width=9,font=('Agency FB',14))
    E_nombre4.place(x=430,y=150)
    initialtext5= StringVar()
    initialtext5.set('Bib Fortuna')
    E_nombre5 = Entry(sele,textvariable=initialtext5,width=9,font=('Agency FB',14))
    E_nombre5.place(x=570,y=150)
    initialtext6= StringVar()
    initialtext6.set('Jabba')
    E_nombre6 = Entry(sele,textvariable=initialtext6,width=9,font=('Agency FB',14))
    E_nombre6.place(x=710,y=150)
    initialtext7= StringVar()
    initialtext7.set('Greedo')
    E_nombre7 = Entry(sele,textvariable=initialtext7,width=9,font=('Agency FB',14))
    E_nombre7.place(x=850,y=150)
    
    initialtext8= StringVar()
    initialtext8.set('PondaBaba')
    E_nombre8 = Entry(sele,textvariable=initialtext8,width=9,font=('Agency FB',14))
    E_nombre8.place(x=10,y=390)
    initialtext9= StringVar()
    initialtext9.set('Evazan')
    E_nombre9 = Entry(sele,textvariable=initialtext9,width=9,font=('Agency FB',14))
    E_nombre9.place(x=150,y=390)
    initialtext10= StringVar()
    initialtext10.set('Figrin')
    E_nombre10 = Entry(sele,textvariable=initialtext10,width=9,font=('Agency FB',14))
    E_nombre10.place(x=290,y=390)
    initialtext11= StringVar()
    initialtext11.set('Ree-Yees')
    E_nombre11 = Entry(sele,textvariable=initialtext11,width=9,font=('Agency FB',14))
    E_nombre11.place(x=430,y=390)
    initialtext12= StringVar()
    initialtext12.set('Tessek')
    E_nombre12 = Entry(sele,textvariable=initialtext12,width=9,font=('Agency FB',14))
    E_nombre12.place(x=570,y=390)
    initialtext13= StringVar()
    initialtext13.set('Boba Fett')
    E_nombre13 = Entry(sele,textvariable=initialtext13,width=9,font=('Agency FB',14))
    E_nombre13.place(x=710,y=390)  
    initialtext14= StringVar()
    initialtext14.set('Palpatine')
    E_nombre14 = Entry(sele,textvariable=initialtext14,width=9,font=('Agency FB',14))
    E_nombre14.place(x=850,y=390)

    initialtext15= StringVar()
    initialtext15.set('Bantha')
    E_nombre15 = Entry(sele,textvariable=initialtext15,width=9,font=('Agency FB',14))
    E_nombre15.place(x=70,y=630)
    initialtext16= StringVar()
    initialtext16.set('Anakin')
    E_nombre16 = Entry(sele,textvariable=initialtext16,width=9,font=('Agency FB',14))
    E_nombre16.place(x=220,y=630)
    initialtext17= StringVar()
    initialtext17.set('Imperial')
    E_nombre17 = Entry(sele,textvariable=initialtext17,width=9,font=('Agency FB',14))
    E_nombre17.place(x=370,y=630)
    initialtext18= StringVar()
    initialtext18.set('Stormtrooper')
    E_nombre18 = Entry(sele,textvariable=initialtext18,width=10,font=('Agency FB',14))
    E_nombre18.place(x=520,y=630)
    initialtext19= StringVar()
    initialtext19.set('Scouttrooper')
    E_nombre19 = Entry(sele,textvariable=initialtext19,width=10,font=('Agency FB',14))
    E_nombre19.place(x=670,y=630)  
    initialtext20= StringVar()
    initialtext20.set('Diagonas')
    E_nombre20 = Entry(sele,textvariable=initialtext20,width=9,font=('Agency FB',14))
    E_nombre20.place(x=820,y=630)
   
    
    Btn_1= Button(sele,text="1",image=g1,command=disparo,fg="yellow",bg="Red")
    Btn_1.place(x=10,y=60)
    Btn_1.config( height = 70, width =70)
    Btn_2= Button(sele,text="2",image=g2,command=disparo,fg="yellow",bg="Blue")
    Btn_2.place(x=150,y=60)
    Btn_2.config( height = 70, width =70)
    Btn_3= Button(sele,text="3",image=g3,command=disparo,fg="yellow",bg="Green")
    Btn_3.place(x=290,y=60)
    Btn_3.config( height = 70, width =70)
    Btn_5= Button(sele,text="5",image=g5,command=disparo,fg="yellow",bg="Purple")
    Btn_5.place(x=430,y=60)
    Btn_5.config( height = 70, width =70)
    Btn_6= Button(sele,text="5",image=g6,command=disparo,fg="yellow",bg="Yellow")
    Btn_6.place(x=570,y=60)
    Btn_6.config( height = 70, width =70)
    Btn_7= Button(sele,text="5",image=g7,command=disparo,fg="yellow",bg="Light Blue")
    Btn_7.place(x=710,y=60)
    Btn_7.config( height = 70, width =70)
    Btn_8= Button(sele,text="5",image=g8,command=disparo,fg="yellow",bg="Orange")
    Btn_8.place(x=850,y=60)
    Btn_8.config( height = 70, width =70)
    Btn_9= Button(sele,text="1",image=g9,command=disparo,fg="yellow",bg="darkcyan")
    Btn_9.place(x=10,y=300)
    Btn_9.config( height = 70, width =70)
    Btn_10= Button(sele,text="2",image=g10,command=disparo,fg="yellow",bg="light pink")
    Btn_10.place(x=150,y=300)
    Btn_10.config( height = 70, width =70)
    Btn_11= Button(sele,text="3",image=g11,command=disparo,fg="yellow",bg="aqua")
    Btn_11.place(x=290,y=300)
    Btn_11.config( height = 70, width =70)
    Btn_12= Button(sele,text="5",image=g12,command=disparo,fg="yellow",bg="beige")
    Btn_12.place(x=430,y=300)
    Btn_12.config( height = 70, width =70)
    Btn_13= Button(sele,text="5",image=g13,command=disparo,fg="yellow",bg="olive")
    Btn_13.place(x=570,y=300)
    Btn_13.config( height = 70, width =70)
    Btn_14= Button(sele,text="5",image=g14,command=disparo,fg="yellow",bg="Light Green")
    Btn_14.place(x=710,y=300)
    Btn_14.config( height = 70, width =70)
    Btn_15= Button(sele,text="5",image=g15,command=disparo,fg="yellow",bg="Brown")
    Btn_15.place(x=850,y=300)
    Btn_15.config( height = 70, width =70)
    Btn_16= Button(sele,text="1",image=g16,command=disparo,fg="yellow",bg="coral")
    Btn_16.place(x=70,y=540)
    Btn_16.config( height = 70, width =70)
    Btn_17= Button(sele,text="2",image=g17,command=disparo,fg="yellow",bg="gray")
    Btn_17.place(x=220,y=540)
    Btn_17.config( height = 70, width =70)
    Btn_18= Button(sele,text="3",image=g18,command=disparo,fg="yellow",bg="goldenrod")
    Btn_18.place(x=370,y=540)
    Btn_18.config( height = 70, width =70)
    Btn_19= Button(sele,text="5",image=g19,command=disparo,fg="yellow",bg="white")
    Btn_19.place(x=520,y=540)
    Btn_19.config( height = 70, width =70)
    Btn_20= Button(sele,text="5",image=g20,command=disparo,fg="yellow",bg="turquoise")
    Btn_20.place(x=670,y=540)
    Btn_20.config( height = 70, width =70)
    Btn_3= Button(sele,text="5",image=g4,command=disparo,fg="yellow",bg="violet")
    Btn_3.place(x=820,y=540)
    Btn_3.config( height = 70, width =70)
    
    
    sele.mainloop()
def informacion():
    """Instituto Tecnologico de Costa Rica
    Ingieneria en Computadores
    Programa:info
    Lenguaje Python 3.6.4
    Autor:Daniel Núñez Murillo
    Version 1.0
    Fecha de Ultima Modificacion:Mayo 2/2018
    Entradas:No hay
    Salidas:  Creacion de ventana con informacion
    Restricciones:No hay"""
    root.withdraw()
    datos=Toplevel()
    datos.title('CREDITOS')
    datos.minsize(1000,750)
    datos.resizable(width=NO, height=NO)
    C_dat=Canvas(datos, width=1000,height=750, bg='black')
    C_dat.place(x=0,y=0)
    foto=cargarImagen('img4.gif')
    C_dat.create_image(200,400,image=foto)
    foto2=cargarImagen('bryan.gif')
    C_dat.create_image(400,400,image=foto2)
    def back1():
        datos.destroy()
        root.deiconify()

    about='''Creadores:
Daniel Nuñez Murillo 2018216543
Brayan Rodriguez Villalobos 2018079212
Ingenieria en computadores
    Profesor: Milton Villegas Lemus
    Creado en Costa Rica
    Version 1.0.0
    
    '''
    L_cred = Label(C_dat,text=about,font=('Agency FB',23),bg='black',fg='yellow')
    L_cred.place(x=500,y=300)
    
    Btn_back1 = Button(C_dat,text='Atras',command=back1,bg='Black',fg='yellow')
    Btn_back1.place(x=500,y=700)    
    Btn_back1.config( height = 1, width =13)
    datos.mainloop()
def end():
        """Instituto Tecnologico de Costa Rica
        Ingieneria en Computadores
        Programa:end
        Lenguaje Python 3.6.4
        Autor:Daniel Núñez Murillo
        Version 1.0
        Fecha de Ultima Modificacion:Mayo 2/2018
        Entradas:No hay
        Salidas:Destruccion del juego
        Restricciones:No hay"""
        
        root.destroy()
        winsound.PlaySound(None,winsound.SND_ASYNC)

def puntaltos():
    file=open("C:/Users/meji/Documents/Proyecto2/Proyectogit/puntajes.txt", "w")
    file.write("Primera fila" + os.linesep)
    file.write("segunda")
    file.close()
def instrucciones():  
    Instrucciones="""
Haga click en el botón iniciar, a continuación debe elegir
el personaje con el que jugará. Luego escoja el modo de juego
deseado, "Aros" consiste en maniobrar la nave espacial a través
de los aros. En el modo de juego "Asteroides" debe esquivar los asteroides;
o bien puede destruirlos si les dispara con la tecla de espacio.
Para mover la nave utilice las flechas del teclado"""
    root.withdraw()      
    Inst = Tk()
    Inst.minsize(800,600)
    Inst.title('INSTRUCCIONES')
    Inst.resizable(False,False)
    C_Inst = Canvas(Inst,width=800, height = 600, bg='Black')
    C_Inst.place(x=0,y=0)
    L_Inst = Label(Inst,text=Instrucciones, font=('Agency FB',20) ,bg='black', fg='yellow')
    L_Inst.config(width=70,height=10)
    L_Inst.place(x=80,y=50)


    def atras():
        Inst.withdraw()
        root.deiconify()



    Btn_Back = Button(Inst, text='ATRÁS',command=atras,bg='black',fg='yellow')
    Btn_Back.configure(width=8,height=2)
    Btn_Back.place(x=500,y=550)

    
    Inst.mainloop()
     





Btn_dat= Button(root,text="MEJORES PUNTAJES",command=puntaltos,fg="yellow",bg="Black")
Btn_dat.place(x=1200,y=580)
Btn_dat.config( height = 3, width =20)    
        
Btn_dat= Button(root,text="CREDITOS",command=informacion,fg="yellow",bg="Black")
Btn_dat.place(x=1200,y=650)
Btn_dat.config( height = 3, width =20)        
Btn_song= Button(root,text="CERRAR",command=end,fg="yellow",bg="Black")
Btn_song.place(x=675,y=600)
Btn_song.config( height = 5, width =40)
Btn_song1= Button(root,text="INICIAR",command=seleccion,fg="yellow",bg="Black")
Btn_song1.place(x=675,y=400)
Btn_song1.config( height = 5, width =40)
Btn_Ins = Button(root,text= 'INSTRUCCIONES',command = instrucciones,fg='yellow',bg='Black')
Btn_Ins.place(x=675,y=500)
Btn_Ins.config(height = 5, width =40)
root.mainloop()
