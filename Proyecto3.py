from tkinter import* 
from threading import Thread
import threading
import winsound
import os
import sys
import time
from tkinter import messagebox
import random
global nave
import pygame
from pygame.locals import *
pygame.init()
global x
global y
global energia
global i
global posarox
global posaroy
global puntaje
global aros
global asteroides
aros=9
asteroides=9
puntaje=0
lista=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440]
posarox=random.choice(lista)
posaroy=random.choice(lista)
i=0
energia=7
x=200
y=430




root=Tk()
root.title("Star Wars Arcade")
root.minsize(1400,750)
root.resizable(width=NO,height=NO)
C_root=Canvas(root, width=1400, height=750, bg="black")
C_root.place(x=0,y=0)

def cargarImagen(nombre):
    """
    Instituto Tecnologico de Costa Rica
    Ingieneria en Computadores
    Programa:cargarImagen
    Lenguaje Python 3.6.4
    Autor:Daniel Núñez Murillo
    Version 1.0
    Fecha de Ultima Modificacion:Mayo 2/2018
    Entradas:nombre de la imagen
    Salidas:la imagen en el canvas
    Restricciones: la imagen debe encontrarse en la carpeta img
    """
    ruta = os.path.join('img',nombre)
    imagen = PhotoImage(file=ruta)
    return imagen
def song():
    winsound.PlaySound(None,winsound.SND_ASYNC)
p=threading.Thread(target=song,args=())
p.start()
imagenFondo = cargarImagen("gamefondo.gif")
C_root.create_image(0,0, image = imagenFondo, anchor = NW)


L_ingresarNombre = Label(C_root,text="Ingrese su nombre:",font=('Agency FB',14),bg='black',fg='yellow')
L_ingresarNombre.place(x=730,y=300)
E_nombre = Entry(C_root,width=20,font=('Agency FB',14))
E_nombre.place(x=730,y=350)

def disparo():
    """
Instituto Tecnologico de Costa Rica
Ingieneria en Computadores
Programa:disparo
Lenguaje Python 3.6.4
Autor:Daniel Núñez Murillo
Version 1.0
Fecha de Ultima Modificacion:Mayo 5/2018
Entradas:No hay
Salidas:Se encarga de la creacion de la ventana principal del juego
Restricciones:No hay
"""
    root.withdraw()
    #Pantalla secundari
    info=Toplevel()
    info.title('SELECCION DE NIVEL')
    info.minsize(1400,750)
    info.resizable(width=NO, height=NO)
    nombre_jugador = str(E_nombre.get())

    C_info=Canvas(info, width=1400,height=750, bg='black')
    C_info.place(x=0,y=0)
    L_nombre=Label(C_info, text="Jugador: "+nombre_jugador ,font=('Agency FB',20), fg='yellow', bg='black')
    L_nombre.place(x=10,y=10)

    
    def back():
        
        info.destroy()
        root.deiconify()
    

               
        

    
    
   
   
    
    
        
    #_____________________________________________________________________________________
      #Se pone siempre que se use pygame
        #_____________________________________________________________________________________
        #Variables que permiten que sea más eficiente el control de algunas opciones para el juego
    ANCHO_PANTALLA= 800
    ALTO_PANTALLA= 600
    #_____________________________________________________________________________________
    #___________________________________
    #Se definen los códigos RGB de algunos colores
    white=(255,255,255)
    black=(0,0,0)
        #_____________________________________________________________________________________
        #Se define una clase para crear y manejar el movimiento de la nave en la pantalla


    

    fondo=pygame.image.load('fondo.gif')
    naveImg = pygame.image.load('Nave.gif')
    asteroide = pygame.image.load('Asteroide.png')
    def pygame1():
        
        def iniciar():
            info.withdraw()
            
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.blit(fondo,(0,0))
            
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
            pygame.display.update()
       
        
        def nave(x,y):
            info.withdraw()
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.blit(fondo,(0,0))
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
                
            Pantalla.blit(naveImg,(x,y))
            pygame.display.update()
        def mov_nave(event):
            global x,y
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.fill(black)
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
            if x+10<450 and event  == "RIGHT":
                x = x + 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif x-10>0 and event == "LEFT":
                x = x-10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif y-10>0 and event == "UP":
                y = y - 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif y+10<450 and event == "DOWN":
                y = y + 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif event == 'shot':
                 winsound.PlaySound('fire.wav',winsound.SND_ASYNC)
                
        def main(Modo):
            iniciar()        
            nave(200,430)
            global energia,i,posarox,posaroy,puntaje,aros
            if Modo == "Aros":
                while energia>0:
                    i=i+1
                    if i%300==0:
                        posarox=random.choice(lista)
                        posaroy=random.choice(lista)
                        aros=aros-1
                        
                    elif i%299==0:
                        if (x,y)==(posarox,posaroy):
                            puntaje=puntaje+1
                            
                        else:
                            quit()
                    elif i%200==0:
                        energia=energia-1    
                    elif i%250==0:
                        
                            en=random.choice(['no','yes'])
                            if en=='no':
                                energia=energia
                            else:
                                print ('recarga')
                                energia=energia+1
                    
                        
                        
                    
                    print ('x=',x,'y=',y,energia,posarox,posaroy,puntaje,aros)
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_RIGHT:
                                mov_nave("RIGHT")
                            if event.key == pygame.K_LEFT:
                                mov_nave("LEFT")
                            if event.key == pygame.K_UP:
                                mov_nave("UP")
                            if event.key == pygame.K_DOWN:
                                mov_nave("DOWN")
                            if event.key == pygame.K_SPACE:
                                mov_nave('shot')
        
            if Modo == "Asteroides":
                while nave:
                    
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_RIGHT:
                                mov_nave("RIGHT")
                            if event.key == pygame.K_LEFT:
                                mov_nave("LEFT")
                            if event.key == pygame.K_UP:
                                mov_nave("UP")
                            if event.key == pygame.K_DOWN:
                                mov_nave("DOWN")
                            
        def cargarImagen(nombre):
            ruta = os.path.join('Proyecto#2',nombre)
            imagen = PhotoImage(file=ruta)
            return imagen
        main('Aros')    
    def pygame2():
        
        def iniciar():
            info.withdraw()
            
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.blit(fondo,(0,0))
            
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
            pygame.display.update()
       
        
        def nave(x,y):
            info.withdraw()
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.blit(fondo,(0,0))
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
                
            Pantalla.blit(naveImg,(x,y))
            pygame.display.update()
        def mov_nave(event):
            global x,y
            Pantalla=pygame.display.set_mode((ANCHO_PANTALLA,ALTO_PANTALLA))
            pygame.display.set_caption("Pantalla de Juego")
            Pantalla.fill(black)
            clock= pygame.time.Clock()
            clock.tick(60)#Estos son los fps para que se actualice la pantalla
            if x+10<450 and event  == "RIGHT":
                x = x + 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif x-10>0 and event == "LEFT":
                x = x-10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif y-10>0 and event == "UP":
                y = y - 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif y+10<450 and event == "DOWN":
                y = y + 10
                Pantalla.fill(black)
                pygame.display.update(nave(x,y))
            elif event == 'shot':
                 winsound.PlaySound('fire.wav',winsound.SND_ASYNC)
                
        def main(Modo):
            iniciar()        
            nave(200,430)
            global energia,i,posarox,posaroy,puntaje,asteroides
            if Modo == "Aros":
                while energia>0:
                    i=i+1
                    if puntaje==9:
                        quit()
                    if i%300==0:
                        posarox=random.choice(lista)
                        posaroy=random.choice(lista)
                        
                    elif i%299==0:
                        if (x,y)==(posarox,posaroy):
                            puntaje=puntaje+1
                            
                        else:
                            quit()
                    elif i%200==0:
                        energia=energia-1    
                    elif i%250==0:
                        
                            en=random.choice(['no','yes'])
                            if en=='no':
                                energia=energia
                            else:
                                print ('recarga')
                                energia=energia+1
                    
                        
                        
                    
                    print ('x=',x,'y=',y,energia,posarox,posaroy,puntaje)
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_RIGHT:
                                mov_nave("RIGHT")
                            if event.key == pygame.K_LEFT:
                                mov_nave("LEFT")
                            if event.key == pygame.K_UP:
                                mov_nave("UP")
                            if event.key == pygame.K_DOWN:
                                mov_nave("DOWN")
                            if event.key == pygame.K_SPACE:
                                mov_nave('shot')
        
            if Modo == "Asteroides":
                while energia>0:
                    i=i+1
                    if i>2750:
                        quit()
                    if puntaje==9:
                        quit()
                    if i%300==0:
                        posarox=random.choice(lista)
                        posaroy=random.choice(lista)
                        asteroides=asteroides-1
                    elif (x,y)==(posarox,posaroy) and event.key == pygame.K_SPACE:
                        mov_nave('shot')
                        puntaje=puntaje+1
                        posarox=random.choice(lista)
                        posaroy=random.choice(lista)
                        asteroides=asteroides+1
                        
                        
                    elif i%299==0:
                        if (x,y)==(posarox,posaroy):
                            quit()
                        else:
                            puntaje=puntaje
                            
                        
                    elif i%200==0:
                        energia=energia-1    
                    elif i%250==0:
                        
                            en=random.choice(['no','yes'])
                            if en=='no':
                                energia=energia
                            else:
                                print ('recarga')
                                energia=energia+1
                    print ('x=',x,'y=',y,energia,posarox,posaroy,puntaje,asteroides)
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            quit()
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_RIGHT:
                                mov_nave("RIGHT")
                            if event.key == pygame.K_LEFT:
                                mov_nave("LEFT")
                            if event.key == pygame.K_UP:
                                mov_nave("UP")
                            if event.key == pygame.K_DOWN:
                                mov_nave("DOWN")
                            if event.key == pygame.K_SPACE:
                                mov_nave('shot')    
                            
        def cargarImagen(nombre):
            ruta = os.path.join('Proyecto#2',nombre)
            imagen = PhotoImage(file=ruta)
            return imagen
        main('Asteroides')                        
    imagenGri = cargarImagen("fondo.gif")
    C_info.create_image(0,0, image = imagenGri, anchor = NW)
    Btn_back = Button(info,text='AROS',bg='Black',command=pygame1,fg='yellow')
    Btn_back.place(x=595,y=160)    
    Btn_back.config( height = 7, width =70)
    Btn_back = Button(info,text='ASTEROIDES',bg='Black',command=pygame2,fg='yellow')
    Btn_back.place(x=595,y=360)    
    Btn_back.config( height = 7, width =70)
    Btn_back = Button(info,text='Atras',command=back,bg='Black',fg='yellow')
    Btn_back.place(x=416,y=532)    
    Btn_back.config( height = 1, width =13)
    info.mainloop()

def informacion():
    """Instituto Tecnologico de Costa Rica
    Ingieneria en Computadores
    Programa:info
    Lenguaje Python 3.6.4
    Autor:Daniel Núñez Murillo
    Version 1.0
    Fecha de Ultima Modificacion:Mayo 2/2018
    Entradas:No hay
    Salidas:  Creacion de ventana con informacion
    Restricciones:No hay"""
    root.withdraw()
    datos=Toplevel()
    datos.title('CREDITOS')
    datos.minsize(1000,750)
    datos.resizable(width=NO, height=NO)
    C_dat=Canvas(datos, width=1000,height=750, bg='black')
    C_dat.place(x=0,y=0)
    foto=cargarImagen('img4.gif')
    C_dat.create_image(200,400,image=foto)
    foto2=cargarImagen('bryan.gif')
    C_dat.create_image(400,400,image=foto2)
    def back1():
        datos.destroy()
        root.deiconify()

    about='''Creadores:
Daniel Nuñez Murillo 2018216543
Brayan Rodriguez Villalobos 2018079212
Ingenieria en computadores
    Profesor: Milton Villegas Lemus
    Creado en Costa Rica
    Version 1.0.0
    
    '''
    L_cred = Label(C_dat,text=about,font=('Agency FB',23),bg='black',fg='yellow')
    L_cred.place(x=500,y=300)
    
    Btn_back1 = Button(C_dat,text='Atras',command=back1,bg='Black',fg='yellow')
    Btn_back1.place(x=500,y=700)    
    Btn_back1.config( height = 1, width =13)
    datos.mainloop()
def end():
        """Instituto Tecnologico de Costa Rica
        Ingieneria en Computadores
        Programa:end
        Lenguaje Python 3.6.4
        Autor:Daniel Núñez Murillo
        Version 1.0
        Fecha de Ultima Modificacion:Mayo 2/2018
        Entradas:No hay
        Salidas:Destruccion del juego
        Restricciones:No hay"""
        
        root.destroy()
        winsound.PlaySound(None,winsound.SND_ASYNC)
Btn_dat= Button(root,text="CREDITOS",command=informacion,fg="yellow",bg="Black")
Btn_dat.place(x=1200,y=650)
Btn_dat.config( height = 3, width =20)        
Btn_song= Button(root,text="CERRAR",command=end,fg="yellow",bg="Black")
Btn_song.place(x=675,y=600)
Btn_song.config( height = 5, width =40)
Btn_song1= Button(root,text="INICIAR",command=disparo,fg="yellow",bg="Black")
Btn_song1.place(x=675,y=400)
Btn_song1.config( height = 5, width =40)
root.mainloop()
